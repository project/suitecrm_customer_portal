<?php

/**
 * Manage Page, sidebar and top.
 */

  global $base_url;

  // If set standalone template.
  if (variable_get('dsp_portal_template') == 1) { ?>
  <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN"
    "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">
  <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" version="XHTML+RDFa 1.0" dir="ltr">

    <head profile="http://www.w3.org/1999/xhtml/vocab">
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

      <!--    <link type="text/css" rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css" media="all" />-->
    <link type="text/css" rel="stylesheet" href="<?php print $base_url . "/" . drupal_get_path('module', 'drupal_sugar_portal') . '/css/jquery-ui.css'; ?>" media="all" />
    <link type="text/css" rel="stylesheet" href="<?php print $base_url . "/" . drupal_get_path('module', 'drupal_sugar_portal') . '/css/font-awesome.min.css'; ?>" media="all" />


<!--    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js?v=1.7.2"></script>-->
    <script type="text/javascript" src="<?php print $base_url . "/" . drupal_get_path('module', 'drupal_sugar_portal') . '/js/jquery-1.7.2.min.js'; ?>"></script>
    <script>var ajaxURL = "<?php echo $base_url; ?>";</script>
    <script type="text/javascript" src="<?php print $base_url . "/" . drupal_get_path('module', 'drupal_sugar_portal') . '/js/standalone.js'; ?>"></script>
    <script type="text/javascript" src="<?php print $base_url . "/" . drupal_get_path('module', 'drupal_sugar_portal') . '/js/bootstrap.min.js'; ?>"></script>
    <script type="text/javascript" src="<?php print $base_url . "/" . drupal_get_path('module', 'drupal_sugar_portal') . '/js/app.js'; ?>"></script>
    <script type="text/javascript" src="<?php print $base_url . "/" . drupal_get_path('module', 'drupal_sugar_portal') . '/js/jquery-ui/jquery-ui-1.10.3.custom.min.js'; ?>"></script>
    <script type="text/javascript" src="<?php print $base_url . "/" . drupal_get_path('module', 'drupal_sugar_portal') . '/js/jquery.validate/jquery.validate.js'; ?>"></script>
<?php
  }
  // If set fullwidth template.
  if (variable_get('dsp_portal_template') == 1) { ?>
    <title>Portal | <?php print $_SERVER['SERVER_NAME']; ?></title>
  </head>
  <body class="scp-standalone" >
<?php
  }

    global $_drupal_sugar_portal_objSCP, $_drupal_sugar_portal_sugar_crm_version, $_drupal_sugar_portal_image_url;
    $check_var = drupal_sugar_portal_is_curl();
    if (($check_var == 'yes')) {
      if (isset($_SESSION['scp_user_id']) == TRUE) {
        if (isset($_REQUEST['conerror']) && !empty($_REQUEST['conerror'])) {
          if (isset($_COOKIE['bcp_connection_error']) && $_COOKIE['bcp_connection_error'] != '') {
            $cookie_err = $_COOKIE['bcp_connection_error'];
            unset($_COOKIE['bcp_login_error']);
            return "<div class='error settings-error' id='setting-error-settings_updated'>
      <p><strong>$cookie_err</strong></p>
    </div>";
          }
        }
        // Updated by BC on 16-jun-2016.
        $check_token = '';
        if ($_drupal_sugar_portal_sugar_crm_version == 6) {
          if (is_object($_drupal_sugar_portal_objSCP)) {
            $check_token = $_drupal_sugar_portal_objSCP->sessionId;

            $list_result_count_all = $_drupal_sugar_portal_objSCP->get_relationships('Contacts', $_SESSION['scp_user_id'], 'cases', array('id'), '', '', 0);
            if (!empty($list_result_count_all->entry_list)) {
                $countCases = count($list_result_count_all->entry_list);
            } else {
                $countCases = 0;
            }
          }
        }
        if ($_drupal_sugar_portal_sugar_crm_version == 7) {
          if (is_object($_drupal_sugar_portal_objSCP)) {
            $check_token = $_drupal_sugar_portal_objSCP->access_token;

            $list_result_count_all = $_drupal_sugar_portal_objSCP->getRelationship('Contacts', $_SESSION['scp_user_id'], 'cases', 'id', array(), '', '', 'date_entered:DESC');
            if (!empty($list_result_count_all->records)) {
                $countCases = count($list_result_count_all->records);
            } else {
                $countCases = 0;
            }
          }
        }
        if ($check_token != '') {
          require drupal_get_path('module', 'drupal_sugar_portal') . '/inc/common_connection_check.inc';

          // If set fullwidth template.
          if (variable_get('dsp_portal_template') == 1) {   ?>
            <div class="scp-header">
              <div class="scp-container">
                  <h1 class="scp-page-title"><span class='fa fa-bars scp-bar-toggle'></span>
                    <?php if (variable_get('portal_name') != NULL) {
                      print variable_get('portal_name');
                    }else{
                       print t("Free Portal");
                    } ?>
                  </h1>

              </div>
            </div>
          <?php } ?>
            <div class="scp-main">
              <div class="scp-container">
                <?php
                // If set fullwidth template.
                if (variable_get('dsp_portal_template') != 1) {
                ?>
                  <div class="scp-header">
                    <a href='javascript:void(0)' class=""><div class="scp-title"><h1 class="scp-page-title scp-menu-modules"><span class='fa fa-bars scp-bar-toggle'></span> <?php
                          if (variable_get('portal_name') != NULL) {
                            print variable_get('portal_name');
                          }
                          else {
                            print t("Free Portal");
                          }
                          ?></h1></div></a>
                    <div class='scp-action userinfo'>
                      <a href='javascript:void(0)' class="scp-menu-dashboard"><?php print $_SESSION['scp_user_account_name']; ?> </a>
                      <ul class="scp-open-dashboard-menu">
                        <li><a href='<?php echo $base_url; ?>/dsp-profile'><i class='fa fa-user'></i> <?php print t("My Profile"); ?></a></li>
                        <li><a href='<?php echo $base_url; ?>/dsp-logout'><i class='fa fa-power-off'></i> <?php print t('Log Out'); ?></a></li>
                      </ul>
                    </div>
                  </div>
                <?php } ?>
                <div class="scp-leftpanel">
                <?php
                    $clss = 'scp-open-modules-menu';
                    $countAcc = 0;
                    if(variable_get('dsp_portal_template') != 1){
                      $clss = 'scp-open-modules-menu';
                    }
                ?>
                    <ul class="scp-sidemenu <?php print $clss;?>">
                      <li class="scp-menu-profile-sec"> <?php
                        echo "<div class='scp-user-img'><img src='" . $_drupal_sugar_portal_image_url . "/images.png' width='50' height='50'></div>";
                        ?><span class="scp-menu-manage username"> <?php print l($_SESSION['scp_user_account_name'], '/dsp-profile'); ?></span>
                        <a href='<?php echo $base_url; ?>/dsp-profile' class="scp-profile-edit" title="My Profile"><i class="fa fa-pencil"></i> </a>
                        <ul>
                          <li><a class='scp-profile-logout'  href='<?php echo $base_url; ?>/dsp-logout'> <?php print t('Log Out'); ?></a></li>
                        </ul>
                      </li>
                      <li id="Cases_id" class="scp-sidebar-class">
                        <a class="label"> <span class="fa Cases side-icon-wrapper"></span> &nbsp;<span class="menu-text">Cases</span><span class="fa arrow"></span></a>
                        <ul id="dropdown_Cases_id" class="inner_ul">
                          <li style="list-style: none;" id="edit-Cases">
                            <a class="link-class" href="javascript:void(0);" onclick="drupal_sugar_portal_ajax_load('edit', 'Cases')" id="edit-Cases"><span class="fa fa-plus side-icon-wrapper"></span><span>Add Case</span></a>
                          </li>
                          <li style="list-style: none;" id="list-Cases">
                            <a class="link-class " href="javascript:void(0);" onclick="drupal_sugar_portal_ajax_load('list', 'Cases')" id="list-Cases"><span class="fa fa-list side-icon-wrapper"></span><span>View Cases</span></a>
                          </li>
                        </ul>
                      </li>
                    </ul>
                  </div>
                  <div class="scp-rightpanel">
                    <div id="ajax-target" class="dsp-response"></div>
                    <span id='dsp-loader'><img src="<?php print $_drupal_sugar_portal_image_url; ?>/ajax-loading.gif"></span>
                    <div id="otherdata" style="display:none;"></div>
                  </div>
              </div>
            </div>
          <?php
        } else {
          drupal_set_message(t('Connection with CRM not successful. Please check CRM Version, URL, Username and Password.'), 'error');
        }
      } else {
        drupal_goto('/dsp-login');
      }
    } else {//CURL exist CHECKING
      drupal_get_messages('error');
      drupal_set_message($check_var, 'error');
      return FALSE;
    }
    ?>
    <script>
      //mange page script
      var ajaxURL = '<?php print $base_url; ?>';
    </script>
    <link type="text/css" rel="stylesheet" href="<?php
      print $base_url . "/" . drupal_get_path('module', 'drupal_sugar_portal') . '/css/dsp-style.css';
      ?>" media="all" />
    <?php
    // If set standalone template.
    if (variable_get('dsp_portal_template') == 1) {
    ?>
    </body>
  </html>
<?php
    }
