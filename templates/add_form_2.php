<?php
parse_str($_REQUEST['form'], $form_data);
//print_r($form_data);
global $objSCP;
global $TEMPLATE_PATH;
        $logged_user_id = $_SESSION['scp_user_id'];
        $id = stripslashes_deep($form_data['id']);
        $module_name = stripslashes_deep($form_data['module_name']);
        $view = stripslashes_deep($form_data['view']);
        $module_without_s = lcfirst(rtrim($module_name, 's'));
        $module_with_s = lcfirst($module_name);

        if (is_array((array) $_SESSION['module_array'])) {//Added for getting module key OR lable added from sugar side
        $arry_lable = (array) $_SESSION['module_array'];
        $module_name_label = $arry_lable[$module_name];
    }
        $module_with_caps = $module_name_label;
        //$module_with_caps = rtrim($module_name, 's');
//        if ($module_name == "Opportunities") {
//            $module_with_caps = "Opportunity";
//        } else {
//            $module_with_caps = rtrim($module_name, 's');
//        }

        if (($module_name == 'Meetings' || $module_name == 'Calls') && array_key_exists('date_start', $form_data)) {
            if ($form_data['date_start'] && $form_data['date_end']) {
                $date1 = new DateTime($form_data['date_start']);
                $date2 = new DateTime($form_data['date_end']);
                $diff = $date2->diff($date1);
                $hours = $diff->h;
                $hours = $hours + ($diff->days * 24);

                $form_data['duration_hours'] = $hours;
                $form_data['duration_minutes'] = $diff->i;
            } else {
                $form_data['duration_hours'] = 1;
                $form_data['duration_minutes'] = 0;
            }
        }
        if (isset($id) && !empty($id)) {
            $id = $id;
            if (isset($form_data['delete']) && !empty($form_data['delete']) && $form_data['delete'] == 1) {
                $deleted = 1;
            }
        } else {
            $id = '';
            $deleted = 0;
        }
        //get name array
        $pass_name_arry = array();
        $results = $objSCP->get_customlayout($module_name, $view);
        include( $TEMPLATE_PATH . 'bcp_add_records_v7.php');
        $pass_name_arry['id'] = $id;
        $pass_name_arry['deleted'] = $deleted;
        if (($module_name == 'Meetings' || $module_name == 'Calls') && array_key_exists('date_start', $form_data)) {
            $pass_name_arry['duration_hours'] = stripslashes_deep($form_data['duration_hours']);
            $pass_name_arry['duration_minutes'] = stripslashes_deep($form_data['duration_minutes']);
        }
        if ($module_name == "Documents") {
            // if document delete request
            if ($deleted == 1) {
                $new_id = $objSCP->set_entry($module_name, $pass_name_arry);
            } else {
                // Document Set Entry
                $upload_file = $_FILES['filename']['name'];
                $upload_path = $_FILES['filename']['tmp_name'];
                $new_id2 = $objSCP->createDocument($pass_name_arry, $upload_file, $upload_path);
                $new_id = $new_id2->record->id;
            }
        } else if ($module_name == "Notes") {
            if ($deleted == 1) {
                $new_id = $objSCP->set_entry($module_name, $pass_name_arry);
            } else { // File Set Entry
                $upload_file = $_FILES['filename']['name'];
                $upload_path = $_FILES['filename']['tmp_name'];
                $new_id2 = $objSCP->createNote($pass_name_arry, $upload_file, $upload_path);
                $new_id = $new_id2->record->id;
                $rel_id = $objSCP->set_relationship('Cases', $form_data['case_id'], strtolower($module_name), $new_id);
            }
        } else {
            $new_id = $objSCP->set_entry($module_name, $pass_name_arry);
        }

        $rel_id = $objSCP->set_relationship('Contacts', $logged_user_id, strtolower($module_name), $new_id);

        if (isset($id) && !empty($id)) {
            if (isset($form_data['delete']) && !empty($form_data['delete']) && $form_data['delete'] == 1) {
                $view1 = 'list';
                $redirect_url = $current_url . '?' . $view1 . '-' . $module_name . '';
                $conn_err = $module_with_caps . " Deleted Successfully";
                setcookie('bcp_add_record', $conn_err, time() + 3600, '/');
                echo $redirect_url;
                wp_die();
            } else {
                $view1 = 'list';
                $redirect_url = $current_url . '?' . $view1 . '-' . $module_name . '';
                $conn_err = $module_with_caps . " Updated Successfully";
                setcookie('bcp_add_record', $conn_err, time() + 3600, '/');
            }
        } else {
            if ($module_name == "Notes" && !empty($form_data['case_id'])) {
                //$redirect_url = $current_url . '?scp-page=add-case&id=' . $form_data['case_id'] . '&view=1&success=true';
                $view1 = 'list';
                $module_name1 = 'Cases';
                $redirect_url = $current_url . '?' . $view1 . '-' . $module_name1 . '';
                $conn_err = $module_with_caps . " Added Successfully In Case";
                setcookie('bcp_add_record', $conn_err, time() + 3600, '/');
            } else {

                $view1 = 'list';
                $redirect_url = $current_url . '?' . $view1 . '-' . $module_name . '';
                $conn_err = $module_with_caps . " Added Successfully";
                setcookie('bcp_add_record', $conn_err, time() + 3600, '/');
            }
        }
        return 'list-'.$module_name;