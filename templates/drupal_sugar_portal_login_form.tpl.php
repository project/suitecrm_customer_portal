<?php

/**
 * Login Form
 */

  global $base_url;
  if (variable_get('dsp_portal_template') == 1) {
?>
  <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN"
    "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">
  <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" version="XHTML+RDFa 1.0" dir="ltr">

    <head profile="http://www.w3.org/1999/xhtml/vocab">
      <?php
      $site_name = $_SERVER['SERVER_NAME'];
      $url = current_path();
      $tokens = explode('/', $url);
      $tokens_dash = explode('-', $tokens[count($tokens) - 1]);
      ?>
      <title><?php
        if (isset($tokens_dash) && $tokens_dash[1] == 'forgot') {
          print 'Portal Forgot Password | ' . $site_name;
        }
        elseif(isset($tokens_dash) && $tokens_dash[1] == 'login'){
          print 'Portal Login | ' . $site_name;
        }
        elseif(isset($tokens_dash) && $tokens_dash[1] == 'signup'){
          print 'Portal Sign Up | ' . $site_name;
        }
        else {
          print 'My Profile | ' . $site_name;
        }
        ?></title>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
      <link type="text/css" rel="stylesheet" href="<?php print $base_url . "/" . drupal_get_path('module', 'drupal_sugar_portal') . '/css/font-awesome.min.css'; ?>" media="all" />
<?php
  }
?>

    <!--    <link type="text/css" rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css" media="all" />-->
    <link type="text/css" rel="stylesheet" href="<?php print $base_url . '/modules/system/system.theme.css'; ?>" media="all" />
    <script type="text/javascript" src="<?php print $base_url . "/" . drupal_get_path('module', 'drupal_sugar_portal') . '/js/jquery-1.7.2.min.js'; ?>"></script>
    <script>var ajaxURL = "<?php echo $base_url; ?>";</script>
    <script type="text/javascript" src="<?php print $base_url . "/" . drupal_get_path('module', 'drupal_sugar_portal') . '/js/jquery.validate/jquery.validate.js'; ?>"></script>
 <?php
  if (variable_get('dsp_portal_template') == 1) { ?>
      <script type="text/javascript" src="<?php print $base_url . "/" . drupal_get_path('module', 'drupal_sugar_portal') . '/js/standalone.js'; ?>"></script>
      <script type="text/javascript" src="<?php print $base_url . "/" . drupal_get_path('module', 'drupal_sugar_portal') . '/js/bootstrap.min.js'; ?>"></script>
    </head>
    <body class="html not-front not-logged-in no-sidebars page-dsp-login scp-standalone">
      <?php if (isset($tokens_dash) && $tokens_dash[1] == 'profile') { ?>
        <div class="scp-header">
          <div class="scp-container">
            <div class="scp-title"><h1 class="scp-page-title"><?php print t("My Profile"); ?></h1></div>
            <div class="scp-action">
              <a href='javascript:void(0)' class="scp-menu-dashboard"><?php print $_SESSION['scp_user_account_name']; ?> </a>
              <ul class="scp-open-dashboard-menu">
                <li><a href='<?php echo $base_url; ?>/dsp-manage-page'><i class='fa fa-bars side-icon-wrapper'></i> <?php print t("Manage Page"); ?></a></li>
                <li><a href='<?php echo $base_url; ?>/dsp-logout'><i class='fa fa-power-off'></i> <?php print t("Log Out"); ?></a></li>
              </ul>
            </div>
          </div>
        </div>
        <?php
      }
      if (isset($tokens_dash) && $tokens_dash[1] == 'signup') {
        ?>
        <div class="scp-header scp-header-home">
          <div class="scp-container">
            <div class="scp-title"><h1 class="scp-page-title"><?php print t("Portal Sign Up"); ?></h1></div>
            <div class="scp-action">
              <a href="<?php print $base_url; ?>/dsp-login" class="scp-btn scp-btn-default"><i class="fa fa-user"></i> Login</a>
            </div>
          </div>
        </div>
        <?php
      }
  }
    ?>

    <div class="crm_standalone">
      <?php
      $old_msg = drupal_get_messages();
      $err_cls = $suc_cls = '';
      if (isset($old_msg['error'][0]) && $old_msg['error'][0] != '') {
        if (variable_get('dsp_portal_template') == 1) {
          $err_cls = 'dsp_error_msg';
        }
        print '<div class="messages error ' . $err_cls.'">' . $old_msg['error'][0] . '</div>';
      }
      if (isset($old_msg['status'][0]) && $old_msg['status'][0] != '') {
        if (variable_get('dsp_portal_template') == 1) {
          $suc_cls = 'success';
        }else{
          $suc_cls = 'status';
        }
        print '<div class="messages ' . $suc_cls . '">' . $old_msg['status'][0] . '</div>';
      }
      print $arg_return_couple_form; ?>
    </div>
      <script type="text/javascript" src="<?php print $base_url . "/" . drupal_get_path('module', 'drupal_sugar_portal') . '/js/bootstrap.min.js'; ?>"></script>
      <script type="text/javascript" src="<?php print $base_url . "/" . drupal_get_path('module', 'drupal_sugar_portal') . '/js/jquery-ui/jquery-ui-1.10.3.custom.min.js'; ?>"></script>

    <script type="text/javascript">
      jQuery("#drupal-sugar-portal-login-form").validate();
      jQuery("#drupal-sugar-portal-forgot-password-form").validate();
      jQuery("#drupal-sugar-portal-signup-form").validate();
      jQuery("#drupal-sugar-portal-profile-form").validate();
    </script>
    <link type="text/css" rel="stylesheet" href="<?php print $base_url . "/" . drupal_get_path('module', 'drupal_sugar_portal') . '/css/dsp-style.css'; ?>" media="all" />

    <?php
      if (variable_get('dsp_portal_template') == 1) {
    ?>
    </body>
  </html>
<?php
      }
