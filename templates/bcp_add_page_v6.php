<?php

/**
 * Add Page for Sugar6.
 */

// Count cases per user.
$list_result_count_all = $_drupal_sugar_portal_objSCP->get_relationships('Contacts', $_SESSION['scp_user_id'], 'cases', array('id'), '', '', 0);
if (!empty($list_result_count_all->entry_list)) {
  $countCases = count($list_result_count_all->entry_list);
}
else {
  $countCases = 0;
}

if ($countCases < 5) {
    $html .= "<div class='scp-form-title scp-Cases-font'>
  <h3>Add New Case</h3>
    <div class='scp-move-action-btn'>
      <a id='clear_btn_id' onclick='bcp_clear_search_txtbox(0,\"Cases\",\"\",\"\",\"\",\"list\",\"$current_url\");' href='javascript:void(0);'  class='scp-Cases scp-dtl-viewbtn scp-btn scp-btn-default'><span class='fa fa-list' ></span><span>LIST</span></a></div>
    </div>
    <div class='scp-form scp-form-2-col'>
          <form method='post' enctype='multipart/form-data' id='general_form_id'>
          <div class='scp-form-container'>
    <div class='panel Overview scp-dtl-panel'><div class='scp-col-12 panel-title'><span class='panel_name'>OverView</span></div>
    <div class='scp-col-6'>
      <div class='scp-form-group'>
          <label><b>Subject</b> <span class='form-required' title='This field is required.'>*</span></label>
          <span><input class='input-text scp-form-control avia-datepicker-div' type='text' name='add-name' id='add-name' required /> </span>
      </div>
    </div>
    <div class='scp-col-6'>
      <div class='scp-form-group'>
          <label><b>Type</b></label>
            <select class='input-text scp-form-control' title='' id='add-type' name='add-type'>
              <option value='Administration' label='Administration'>Administration</option>
              <option value='Product' label='Product'>Product</option>
              <option value='User' label='User'>User</option>
            </select>
      </div>
     </div>
    <div class='scp-col-6'>
        <div class='scp-form-group'>
          <label><b>Priority</b></label>
            <select class='input-text scp-form-control' title='' id='add-priority' name='add-priority'>
              <option value='P1' label='High'>High</option>
              <option value='P2' label='Medium'>Medium</option>
              <option value='P3' label='Low'>Low</option>
            </select>
        </div>
     </div>
     <div class='scp-col-6'>
      <div class='scp-form-group'>
          <label><b>Description</b> <span class='form-required' title='This field is required.'>*</span></label>
          <span><textarea class='input-text scp-form-control' id='add-description' name='add-description' required></textarea></span>
      </div>
     </div>
  </div>
  <div class='scp-form-actions'>
        <input type='hidden' name='module_name' value='Cases'>
        <input type='hidden' name='view' value='edit'>
  <span><input type='submit' value='Submit' class='hover active scp-button'  id='sumbit_add_btn'/>&nbsp&nbsp<input type='button' value='Cancel' class='hover active scp-cancel' onclick=\"drupal_sugar_portal_ajax_load('list', 'Cases')\" id=\"list-Cases\"/></span>
  </div>
        </div>
        </form>
        </div>";

$html .= '<script type="text/javascript">
    jQuery(document).ready(function() {
      //binds to onchange event of your input field
      jQuery("#general_form_id").validate();
      jQuery("form#general_form_id").submit(function(e){
        var isvalidate=jQuery("#general_form_id").valid();
        if(isvalidate){
          jQuery("#dsp-loader").show();
          jQuery("input[type=\"submit\"]").prop("disabled", true);
          var formData = new FormData(jQuery(this)[0]);
          jQuery.ajax({
            url: ajaxURL+"/dsp-manage-page/add",
            type: "POST",
            data: formData,
            async: false,
            success: function (data) {
              jQuery("#dsp-loader").hide();
              jQuery("input[type=\"submit\"]").prop("disabled", true);
              var split_data = data.split("-");
              if(split_data[2] == "specialcasenoteid"){//Update by BC on 07-sep-2016 for case note edit on that page
                var cn_id = jQuery("input[name=\"case_id\"]").val();
                bcp_module_call_view(split_data[1], cn_id, "detail");
              }else{
                drupal_sugar_portal_ajax_load(split_data[0],split_data[1],1);
              }
            },
            cache: false,
            contentType: false,
            processData: false
          });
        }
        e.preventDefault();
        return false;
      });
    });
  </script>';
} else {
     $html .= "<span class='messages error dsp_error_msg'>You are not authorised to add new case. Please contact your administrator.</span>";
 }