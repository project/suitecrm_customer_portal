<?php

/**
 * List  Page for Sugar7.
 */

$html .= "<tr class='row main-col'>";
$html .= "<th style='width: 150px;'>Case Number</th>";
$html .= "<th>Subject</th>";
$html .= "<th>Date Created</th>";
$html .= "<th>Priority</th>";
$html .= "<th>Status</th>";
$html .= "<th class='action-th'><a>Action</a></th>";
$html .= "</tr>";

foreach ($list_result->records as $setAllCases) {
  $id = $setAllCases->id;
  if ($setAllCases->priority == 'P1') {
    $priority = 'High';
  }
  elseif ($setAllCases->priority == 'P2') {
    $priority = 'Medium';
  }
  elseif ($setAllCases->priority == 'P3') {
    $priority = 'Low';
  }
  else {
    $priority = $setAllCases->priority;
  }
  $html .= "<tr>
        <td>" . $setAllCases->case_number . "</td>
        <td>" . $setAllCases->name . "</td>
        <td>" . date('d-m-Y', strtotime($setAllCases->date_entered)) . "</td>
        <td>" . $priority . "</td>
        <td>" . $setAllCases->status . "</td>
        <td class='action edit'><a href='javascript:void(0);' onclick='bcp_module_call_view(\"Cases\",\"$id\",\"detail\",\"\");'><span class='fa fa-eye' title='View'></span></a></td>
      </tr>";
}
