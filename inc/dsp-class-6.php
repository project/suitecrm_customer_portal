<?php

/**
 * Drupal sugar6 connection class file.
 */
class DrupalSugarPortalSugarRestApiCall {

  public $username;
  public $password;
  public $url;
  public $sessionId;
  public $userId;
  public $dateFormat;
  public $timeFormat;

  /**
   * For connection
   */

  public function __construct($url, $username, $password) {
    $this->url = $url;
    $this->username = $username;
    $this->password = $password;
    $login_response = $this->login();
    if (isset($login_response->id) && !empty($login_response->id)) {
      $this->sessionId = $login_response->id;
    }
    if (isset($login_response->name_value_list->userId->value) && !empty($login_response->name_value_list->userId->value)) {
      $this->userId = $login_response->name_value_list->userId->value;
    }
    if (isset($login_response->name_value_list->user_default_dateformat->value) && !empty($login_response->name_value_list->user_default_dateformat->value)) {
      $this->dateFormat = $login_response->name_value_list->user_default_dateformat->value;
    }
    if (isset($login_response->name_value_list->user_default_timeformat->value) && !empty($login_response->name_value_list->user_default_timeformat->value)) {
      $this->timeFormat = $login_response->name_value_list->user_default_timeformat->value;
    }
  }

  /**
   * Call to CRM
   */

  public function call($method, $parameters, $url) {
    ob_start();
    $curl_request = curl_init();

    curl_setopt($curl_request, CURLOPT_URL, $url);
    curl_setopt($curl_request, CURLOPT_POST, 1);
    curl_setopt($curl_request, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
    curl_setopt($curl_request, CURLOPT_HEADER, 1);
    curl_setopt($curl_request, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($curl_request, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl_request, CURLOPT_FOLLOWLOCATION, 0);

    $jsonEncodedData = json_encode($parameters);

    $post = array(
      "method" => $method,
      "input_type" => "JSON",
      "response_type" => "JSON",
      "rest_data" => $jsonEncodedData,
    );

    curl_setopt($curl_request, CURLOPT_POSTFIELDS, $post);
    $result = curl_exec($curl_request);
    curl_close($curl_request);

    if ( ( $result != false ) ) {
        $result = explode("\r\n\r\n", $result, 2);
        $response = json_decode($result[1]);
    } else {
        $response = null;

    }
    ob_end_flush();

    return $response;
  }

  /**
   *  Added by ketul on 05-aug-2015 for login with admin user.
   */

  public function login() {
    $login_parameters = array(
      "user_auth" => array(
        "user_name" => $this->username,
        "password" => md5($this->password),
      ),
    );
    $login_response = $this->call('login', $login_parameters, $this->url);
    return $login_response;
  }

  /**
   * login into Portal (login call it give all information about contact).
   */

  public function portalLogin($username, $password) {
    $get_entry_list = array(
      'session' => $this->sessionId,
      'module_name' => 'Contacts',
      'query' => "username_c = '{$username}' AND  password_c = '{$password}'",
      'order_by' => '',
      'offset' => 0,
      'select_fields' => array('id',
        'username_c',
        'password_c',
        'salutation',
        'first_name',
        'last_name',
        'email1',
        'account_id',
        'title',
        'phone_work',
        'phone_mobile',
        'phone_fax',
      ),
      'max_results' => 0,
    );
    $get_entry_response = $this->call("get_entry_list", $get_entry_list, $this->url);
    return $get_entry_response;
  }

  /**
   * Set Enery Call.
   */

  public function setEntry($module_name, $set_entry_dataArray) {
    if ($module_name == 'Contacts') {
      $set_entry_dataArray['enable_portal_c'] = '1';
    }

    if ($this->sessionId) {
      $nameValueListArray = array();
      $i = 0;
      foreach ($set_entry_dataArray as $field => $value) {
        $nameValueListArray[$i]['name'] = $field;
        $nameValueListArray[$i]['value'] = $value;
        $i++;
      }
      $set_entry_parameters = array(
        "session" => $this->sessionId,
        "module_name" => $module_name,
        "name_value_list" => $nameValueListArray,
      );
      $set_entry_result = $this->call("set_entry", $set_entry_parameters, $this->url);
      $recordID = (isset($set_entry_result->id)) ? $set_entry_result->id : NULL;
      return $recordID;
    }
  }

  /**
   * Get Module Call.
   */

  public function getModuleFields($module_name, $fields = array()) {
    if ($this->sessionId) {
      $parameters = array(
        'session' => $this->sessionId,
        'module_name' => $module_name,
        'fields' => $fields
      );
      $result = $this->call("get_module_fields", $parameters, $this->url);
      return $result;
    }
  }

  /**
   * Get Entry List Call.
   */

  public function get_entry_list($module_name, $where_condition = '', $select_fields_array = array(), $order_by = '', $deleted = 0, $limit = '', $offset = '') {
    if ($this->sessionId) {
      $get_entry_list_parameters = array(
        'session' => $this->sessionId,
        'module_name' => $module_name,
        'query' => $where_condition,
        'order_by' => $order_by,
        "offset" => $offset,
        'select_fields' => $select_fields_array,
        'link_name_to_fields_array' => array(),
        'max_results' => $limit,
        'deleted' => $deleted,
      );
      $get_entry_list_result = $this->call("get_entry_list", $get_entry_list_parameters, $this->url);
      return $get_entry_list_result;
    }
  }

  /**
   * Set Relationship Call.
   */
  public function set_relationship($module_name, $module_id, $relationship_name, $related_ids_array = array(), $deleted = 0) {
    if ($this->sessionId) {
      $set_relationships_parameters = array(
        'session' => $this->sessionId,
        'module_name' => $module_name,
        'module_id' => $module_id,
        'link_field_name' => $relationship_name,
        'related_ids' => $related_ids_array,
        'delete' => $deleted,
      );
      $set_relationships_result = $this->call("set_relationship", $set_relationships_parameters, $this->url);
      return $set_relationships_result;
    }
  }

  /**
   * Get Relationship Call.
   */
  public function get_relationships($module_name, $module_id, $relationship_name, $related_fields_array = array(), $where_condition = '', $order_By = '', $deleted = 0, $offset = 0, $limit = '') {
    if ($this->sessionId) {
      $get_relationships_parameters = array(
        'session' => $this->sessionId,
        'module_name' => $module_name,
        'module_id' => $module_id,
        'link_field_name' => $relationship_name, // Relationship name
        'related_module_query' => $where_condition, // Where condition
        'related_fields' => $related_fields_array,
        'related_module_link_name_to_fields_array' => array(),
        'deleted' => $deleted,
        'order_by' => $order_By,
        'offset' => $offset,
        'limit' => $limit,
      );
      $get_relationships_result = $this->call("get_relationships", $get_relationships_parameters, $this->url);
      return $get_relationships_result;
    }
  }

  /**
   *  Start old functions.
   *  Check user information by username
   */
  public function getUserInformationByUsername($username) {
    $get_entry_list = array(
      'session' => $this->sessionId,
      'module_name' => 'Contacts',
      'query' => "username_c = '{$username}'",
      'order_by' => '',
      'offset' => 0,
      'select_fields' => array('id', 'username_c', 'password_c', 'email1'),
      'max_results' => 0,
    );

    $get_entry_list_result = $this->call("get_entry_list", $get_entry_list, $this->url);
    $isUser = isset($get_entry_list_result->entry_list[0]->name_value_list->username_c->value) ? $get_entry_list_result->entry_list[0]->name_value_list->username_c->value : NULL;
    if ($isUser == $username) {
      return $get_entry_list_result;
    } else {
      return FALSE;
    }

  }
  // End old functions.

  /**
   * Get user information by email address.
   */
  public function getPortalEmailExists($email1) {
    $get_entry_list = array(
      'session' => $this->sessionId,
      'module_name' => 'Contacts',
      'query' => "contacts.id in (
        SELECT eabr.bean_id
        FROM email_addr_bean_rel eabr JOIN email_addresses ea
        ON (ea.id = eabr.email_address_id)
        WHERE eabr.deleted=0 AND ea.email_address = '{$email1}')",
      'order_by' => '',
      'offset' => 0,
      'select_fields' => array('id', 'email1'),
      'max_results' => 0,
    );

    $get_entry_list_result = $this->call("get_entry_list", $get_entry_list, $this->url);
    $isEmailExist = isset($get_entry_list_result->entry_list[0]->name_value_list->email1->value) ? $get_entry_list_result->entry_list[0]->name_value_list->email1->value : NULL;
    if ($isEmailExist == $email1) {
      return TRUE;
    } else {
      return FALSE;
    }

  }

  /**
   * Check user information by username.
   */
  public function getPortalUserInformationByUsername($username) {
    $get_entry_list = array(
      'session' => $this->sessionId,
      'module_name' => 'Contacts',
      'query' => "contacts_cstm.username_c = '{$username}'",
      'order_by' => '',
      'offset' => 0,
      'select_fields' => array('id', 'username_c', 'password_c', 'email1'),
      'max_results' => 0,
    );
    $get_entry_list_result = $this->call("get_entry_list", $get_entry_list, $this->url);
    $isUser = isset($get_entry_list_result->entry_list[0]->name_value_list->username_c->value) ? $get_entry_list_result->entry_list[0]->name_value_list->username_c->value : NULL;
    if ($isUser == $username) {
      return $get_entry_list_result;
    } else {
      return FALSE;
    }
  }

  /**
   * Get User Information.
   */
  public function getPortalUserInformation($user_id) {
    $get_entry_parameters = array(
      'session' => $this->sessionId,
      'module_name' => 'Contacts',
      'id' => $_SESSION['scp_user_id'],
    );
    $get_entry_result = $this->call("get_entry", $get_entry_parameters, $this->url);
    return $get_entry_result;
  }

  /**
   * Added by ketul on 11-aug-2015 for get user timezone.
   */
  public function getUserTimezone() {
    if ($this->sessionId) {
      $parameters = array(
        'session' => $this->sessionId,
        'module_name' => 'Users',
        'user_id' => $this->userId,
      );
      if ($this->sessionId) {
        $get_entry_result = $this->call("getUserTimezone", $parameters, $this->url);
      }
      return $get_entry_result;
    }
  }

}
