jQuery(document).ready(function () {

    mod_name = 'Cases';
    //jQuery("#edit-" + mod_name).show();
    //click on menus

    jQuery('.scp-sidebar-class').live('click', function (e) {

        var that = jQuery(this),
                id = that.attr('id');
        jQuery(".inner_ul").not("#dropdown_" + id).slideUp();
        jQuery('.scp-active-menu').not("#" + id + " a.label").removeClass('scp-active-menu').addClass('scp-sidemenu');

        if (typeof id !== 'undefined')
        {
            var div = '#dropdown_' + id;
            if (jQuery("#" + id + " a.label").hasClass('scp-sidemenu')) {
                jQuery("#" + id + " a.label").removeClass('scp-sidemenu');
            }
            jQuery("#" + id + " a.label").addClass('scp-active-menu');
        }

    });
    //var pathArray = window.location.href.split('/');
    var lastPart = window.location.href.split("/").pop();
    //alert(pathArray[4]);
    if (mod_name == 'calendar') {
        drupal_sugar_portal_ajax_load('view', mod_name);
    } else {
        if(lastPart == 'dsp-dashboard-gui' || lastPart == 'dsp-profile'){

        }else{
            if(lastPart == 'dsp-manage-page'){
                drupal_sugar_portal_ajax_load('list', mod_name);
            }
            else{
                drupal_sugar_portal_ajax_load('list', '');
            }
        }
    }
    jQuery('#succid').hide();
    jQuery('#fail_error').hide();//fail error
    jQuery(".inner_ul> li").click(function () {
        var divId = jQuery(this).attr('id');
        jQuery('.no-toggle').removeAttr('style');
        jQuery('a').removeClass('scp-active-submenu');
        jQuery('#' + divId + ' a').addClass('scp-active-submenu');
    });

    //Side bar toggle
    jQuery(".scp-bar-toggle").click(function () {

        // $("body").toggleClass("toggled-on");
        //jQuery(".scp-leftpanel").toggle("slide");
        jQuery("body").toggleClass("scp-toggled-on", 500);
        jQuery(".scp-container").toggleClass("menu-show", 500);

    });

     //show / hide side bar menu
    jQuery("#toggle").click(function () {
        jQuery(".scp-leftpanel").toggle();
        jQuery(this).toggleClass('toggle-icon-wrapper');
    });
    //Dashboard right dropdown menu
    jQuery('.scp-menu-dashboard').click(function () {
        jQuery('.scp-open-dashboard-menu').slideToggle();
        jQuery(this).toggleClass('menu-active');

    });
    jQuery('.scp-menu-profile').click(function () {
        jQuery('.scp-open-profile-menu').slideToggle();
        jQuery(this).toggleClass('menu-active');
    });
    jQuery('.scp-menu-modules').click(function () {
        jQuery('.scp-open-modules-menu').slideToggle();
        jQuery(this).toggleClass('menu-active');
    });
    var scp_menu_dashboard = jQuery('.scp-menu-dashboard');
    if (jQuery('.scp-open-dashboard-menu').is(":visible")) {
                jQuery('.scp-open-dashboard-menu').hide();
                jQuery(scp_menu_dashboard).toggleClass('menu-active');
    }

    jQuery(window).click(function (e) {
        var scp_menu_manage = jQuery('.scp-menu-manage');
        var scp_menu_modules = jQuery('.scp-menu-modules');
        var scp_menu_dashboard = jQuery('.scp-menu-dashboard');
        var scp_menu_profile = jQuery('.scp-menu-profile');

        if (!scp_menu_manage.is(e.target) // if the target of the click isn't the container...
                && scp_menu_manage.has(e.target).length === 0) // ... nor a descendant of the container
        {
            if (jQuery('.scp-open-manage-menu').is(":visible")) {
                jQuery('.scp-open-manage-menu').hide();
                jQuery(scp_menu_manage).toggleClass('menu-active');
            }
        }
        if (!scp_menu_modules.is(e.target) // if the target of the click isn't the container...
                && scp_menu_modules.has(e.target).length === 0) // ... nor a descendant of the container
        {
            if (jQuery(window).width() > 1024) {
                if (jQuery('.scp-open-modules-menu').is(":visible")) {
                    jQuery('.scp-open-modules-menu').hide();
                }
            }
        }

        if (!scp_menu_dashboard.is(e.target) // if the target of the click isn't the container...
                && scp_menu_dashboard.has(e.target).length === 0) // ... nor a descendant of the container
        {
            if (jQuery('.scp-open-dashboard-menu').is(":visible")) {
                jQuery('.scp-open-dashboard-menu').hide();
                jQuery(scp_menu_dashboard).toggleClass('menu-active');
            }
        }
        if (!scp_menu_profile.is(e.target) // if the target of the click isn't the container...
                && scp_menu_profile.has(e.target).length === 0) // ... nor a descendant of the container
        {
            if (jQuery('.scp-open-profile-menu').is(":visible")) {
                jQuery('.scp-open-profile-menu').hide();
                jQuery(scp_menu_profile).toggleClass('menu-active');
            }
        }
    });
});
jQuery(document).ajaxComplete(function () {
    if (!jQuery('#otherdata').is(':empty'))
    {
        jQuery("#responsedata").after('<div id="otherdata" style="display:none;"></div>');
    }
});
/////////////////Added by BC on 07-jun-2016/////////////////////
var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
            sURLVariables = sPageURL.split('?'),
            sParameterName,
            i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};
///////////////////////////////////////////

function drupal_sugar_portal_ajax_load(view, modulename, add) {
    var li_data = view + "-" + modulename;
    //jQuery('body').removeClass();
    jQuery( 'body' ).removeClass (function (index, css) {
        return (css.match (/(^|\s)scp-\S+/g) || []).join(' ');
    });
    jQuery('li#list-Cases a').addClass('link-class scp-active-submenu');
    jQuery('body').addClass('scp-standalone');
    jQuery('body').addClass('scp-'+modulename+'-body');
    jQuery("#dsp-loader").show();
    jQuery('#otherdata').hide();
    jQuery.ajax({
        url: ajaxURL + "/dsp-manage-page/list",
        type: "post",
        data: {view: view, modulename: modulename, page_no: 0},
        success: function (response) {
            jQuery("#dsp-loader").hide();
            //alert(add == 1);
            jQuery("#ajax-target").html(response);
            if (add == 1) {
                jQuery('#succid').show();
            } else {
                jQuery('#succid').hide();
                jQuery('#fail_error').hide();//fail error
            }
            side_bar_list_ajax(li_data);
        },
        error: function (xhr) {
            //Do Something to handle error
        }
    });
}
function bcp_module_order_by(page, modulename, order_by, order, view, searchval) { // for order by
    page = 0;
    jQuery("#dsp-loader").show();
    jQuery.ajax({
        url: ajaxURL + "/dsp-manage-page/list",
        type: "post",
        data: {view: view, modulename: modulename, page_no: page, 'order_by': order_by, 'order': order, searchval: searchval},
        success: function (response) {
            //Do Something
            jQuery("#dsp-loader").hide();
            jQuery("#ajax-target").html(response);
            jQuery('#succid').hide();
            jQuery('#fail_error').hide();//fail error
        },
        error: function (xhr) {
            //Do Something to handle error
        }
    });
}
function bcp_module_paging(page, modulename, ifsearch, order_by, order, view, get_current_url) { // for paging
    var searchval = jQuery('#search_name').val();
    if (ifsearch == 1 && (searchval.trim() == '' || searchval == null)) {
        alert('Please Enter Keyword To Search');
        return false;
    }
    jQuery("#dsp-loader").show();
    jQuery.ajax({
        url: ajaxURL + "/dsp-manage-page/list",
        type: "post",
        data: {view: view, modulename: modulename, page_no: page, order_by: order_by, order: order, searchval: searchval, current_url: get_current_url},
        success: function (response) {
            //Do Something
            jQuery("#dsp-loader").hide();
            jQuery("#ajax-target").html(response);
            jQuery('#succid').hide();
            jQuery('#fail_error').hide();//fail error
        },
        error: function (xhr) {
            //Do Something to handle error
        }
    });
}
function bcp_clear_search_txtbox(page, modulename, ifsearch, order_by, order, view, get_current_url) { //for clear search text box
    jQuery("#search_name").val('');

    jQuery("#dsp-loader").show();
    jQuery.ajax({
        url: ajaxURL + "/dsp-manage-page/list",
        type: "post",
        data: {view: view, modulename: modulename, page_no: page, order_by: order_by, order: order, current_url: get_current_url},
        success: function (response) {
            jQuery("#dsp-loader").hide();
            jQuery("#ajax-target").html(response);
            jQuery('#otherdata').hide();
            jQuery('#succid').hide();
            jQuery('#fail_error').hide();//fail error
        }
    });
}
function bcp_module_call_view(modulename, id, view) { // for view particular record(detail page)

    jQuery("#dsp-loader").show();
    jQuery.ajax({
        url: ajaxURL + "/dsp-manage-page/list",
        type: "post",
        data: {view: view, modulename: modulename, id: id},
        success: function (response) {
            //Do Something
            jQuery("#dsp-loader").hide();
            //add class on body for perticular module from calendar
            //jQuery('body').removeClass();
            jQuery( 'body' ).removeClass (function (index, css) {
                return (css.match (/(^|\s)scp-\S+/g) || []).join(' ');
            });
            jQuery('body').addClass('scp-standalone');
            jQuery('body').addClass('scp-' + modulename + '-body');
            jQuery("#ajax-target").html(response);
        },
        error: function (xhr) {
            //Do Something to handle error
        }
    });
}
function side_bar_list_ajax(pathId) {

    //Add active class for selected manu
    if (pathId != '') {

        //jQuery("#" + pathId).addClass('scp-active-submenu');
        var pre_id = jQuery("#" + pathId).parent().attr('id');

        jQuery('#' + pre_id + '> li > a').removeClass('scp-active-submenu');
        jQuery('#' + pathId + ' a').addClass('scp-active-submenu');
        var main_li_id = jQuery("#" + pathId).parent().parent().attr('id');

        jQuery("#" + main_li_id + " a.label").addClass('scp-active-menu');

        var ul_id = jQuery("#" + pathId).parent('ul').attr('id');
        jQuery("#" + ul_id).show();

        //hover class
        jQuery('.scp-active-submenu').hover(
                function () {
                    jQuery(this).addClass('scp-active-submenu-hover')
                },
                function () {
                    jQuery(this).removeClass('scp-active-submenu-hover')
                }
        );
        jQuery('.scp-active-menu').hover(
                function () {
                    jQuery(this).addClass('scp-active-menu-hover')
                },
                function () {
                    jQuery(this).removeClass('scp-active-menu-hover')
                }
        );

    } else {
        //jQuery("#list-Accounts").addClass('scp-active-submenu');
        jQuery("#accounts_id a.label").addClass('scp-active-menu');
        jQuery("#dropdown_accounts_id").show();
    }
    //End acive class
}
//update module
function bcp_module_call_add(htmlval, modulename, view, success, deleted, id) { // for add record layout in module

    jQuery('#otherdata').hide();
    htmlval = (typeof htmlval === 'undefined') ? 0 : htmlval;
    success = (typeof success === 'undefined') ? null : success;
    deleted = (typeof deleted === 'undefined') ? null : deleted;
    id = (typeof id === 'undefined') ? null : id;
    jQuery("#dsp-loader").show();
    jQuery.ajax({
        url: ajaxURL + "/dsp-manage-page/list",
        type: "post",
        data: {view: view, modulename: modulename, page_no: 0, id: id},
        success: function (response) {
            jQuery("#dsp-loader").hide();
            if (jQuery.trim(response) != '-1') {
                if (htmlval == 0) {
                    jQuery('#ajax-target').html(response);
                    side_bar_list_ajax(view + "-" + modulename);
                }
                else {
                    jQuery('#otherdata').show();
                    jQuery('#otherdata').html(response);
                    jQuery('#succid').hide();
                    jQuery('#fail_error').hide();//fail error
                }
            }
            else
            {
                var redirect_url = jQuery(location).attr('href') + "?conerror=1";
                window.location.href = redirect_url;
            }
        }
    });
}
//get module list v7
function drupal_sugar_portal_get_module_list_v7()
{
    jQuery('#loader_user_grp').show();
    var p_type = jQuery('#parent_name').val();
    jQuery.ajax({
        url: ajaxURL + "/dsp-manage-page/get_modulev7",
        type: "post",
        data: {parent_name: p_type},
        success: function (response) {
            jQuery('#loader_user_grp').hide();
            jQuery('#related_to_id2').html(response);
        }
    });
}
//get module list v6
function drupal_sugar_portal_get_module_list_v6()
{
    jQuery('#loader_user_grp').show();
    var p_type = jQuery('#parent_name').val();
    jQuery.ajax({
        url: ajaxURL + "/dsp-manage-page/get_modulev6",
        type: "post",
        data: {parent_name: p_type},
        success: function (response) {
            jQuery('#loader_user_grp').hide();
            jQuery('#related_to_id2').html(response);
        }
    });
}