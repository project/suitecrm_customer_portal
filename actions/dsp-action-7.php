<?php

/**
 * Sugar7 all actions will show here.
 */

/**
 * List Page.
 */
function drupal_sugar_portal_bcp_list_module() {

  global $_drupal_sugar_portal_objSCP, $_drupal_sugar_portal_template_path;
  if (is_object($_drupal_sugar_portal_objSCP)) {
    $limit = variable_get('records_per_page');
    $page_no = $_REQUEST['page_no'];
    $offset = ($page_no * $limit) - $limit;
    $where_con = array();

    $list_result = $_drupal_sugar_portal_objSCP->getRelationship('Contacts', (isset($_SESSION['scp_user_id']) ? $_SESSION['scp_user_id'] : ''), 'cases', '', $where_con, '5', '', 'date_entered:DESC');
    $cnt = (isset($list_result->records) ? count($list_result->records) : 0);
    if (!empty($list_result_count_all->records)) {
      $countCases = count($list_result_count_all->records);
    }
    else {
      $countCases = 0;
    }

    $html = "";
    $html .= "<div class='dsp-list-title'><div class='scp-action-header-left'><h3 class='side-icon-wrapper scp-Cases-font scp-default-font'> <i class='side-icon-wrapper fa Cases'></i>CASES</h3>";
    $html .= "<a class='scp-Cases scp-btn scp-btn-default' href='javascript:void(0);' onclick='bcp_module_call_add(0,\"Cases\",\"edit\",\"\",\"\",\"\");'><span class='fa fa-plus side-icon-wrapper'></span><span>ADD CASE</span></a>";
    $html .= "</div>";
    $html .= "</div>";

    if ($cnt > 0) {
      $html .= "<div class='scp-page-action-title'>View Cases </div>";
    }
    if (isset($_COOKIE['bcp_add_record']) && $_COOKIE['bcp_add_record'] != '' && !isset($_COOKIE['bcp_add_record_fail']) && $_COOKIE['bcp_add_record_fail'] == '') {
      $html .= "<span class='success' id='succid'>" . $_COOKIE['bcp_add_record'] . "</span>";
    }
    if (isset($_COOKIE['bcp_add_record_fail']) && $_COOKIE['bcp_add_record_fail'] != '') {
      $html .= "<span class='messages error dsp_error_msg'>" . $_COOKIE['bcp_add_record_fail'] . "</span>";
    }
    if ($cnt > 0) {
      $html .= "<div class='scp-table-responsive'>";
      $html .= "<table id='example' class='display scp-table scp-table-striped scp-table-bordered scp-table-hover' cellspacing='0' width='100%'>";
      include $_drupal_sugar_portal_template_path . "bcp_list_page_v7.php";
      $html .= "</table></div>";
    }
    else {
      $html .= "<div class='scp-table-responsive'>";
      $html .= "<strong class='acp-no-record'>" . t('No Record(s) Found.') . "</strong>";
    }
    $html .= "</div>";
    return $html;
  }
  else {
    $_drupal_sugar_portal_objSCP = 0;
    $conn_err = "Connection with CRM not successful. Please check CRM Version, URL, Username and Password.";
    setcookie('bcp_connection_error', $conn_err, time() + 60, '/');
    return "-1";
  }
}

/**
 * View Page.
 */
function drupal_sugar_portal_bcp_view_module() {

  global $_drupal_sugar_portal_objSCP, $_drupal_sugar_portal_template_path;
  if (is_object($_drupal_sugar_portal_objSCP)) {
    $id = $_POST['id'];
    $html = '';
    if (empty($_SESSION['user_timezone'])) {
      $user_timezone = $_drupal_sugar_portal_objSCP->getUserTimezone();
    }
    else {
      $user_timezone = $_SESSION['user_timezone'];
    }
    date_default_timezone_set($user_timezone->timezone);
    $record_detail = $_drupal_sugar_portal_objSCP->getRecordDetail('Cases', $id);
    include $_drupal_sugar_portal_template_path . "bcp_view_page_v7.php";
    $html .= "</div>";
    echo $html;
  }
  else {
    $_drupal_sugar_portal_objSCP = 0;
    $conn_err = "Connection with CRM not successful. Please check CRM Version, URL, Username and Password.";
    setcookie('bcp_connection_error', $conn_err, time() + 60, '/');
    return "-1";
  }
}


/**
 * For get module list for v7.
 */
function drupal_sugar_portal_get_module_list_v7() {
  global $_drupal_sugar_portal_objSCP;
  if (is_object($_drupal_sugar_portal_objSCP)) {
    $module_name = $_REQUEST['parent_name'];
    $select_fields = "id,name";
    $record_detail = $_drupal_sugar_portal_objSCP->getRelationship('Contacts', $_SESSION['scp_user_id'], strtolower($module_name), $select_fields, '', '', '', 'date_entered:DESC');
    $html = "";
    $html .= "<select id='parent_id' name='parent_id' class='scp-form-control'><option value=''></option>";
    for ($m = 0; $m < count($record_detail->records); $m++) {
      $mod_id = $record_detail->records[$m]->id;
      $mod_name = $record_detail->records[$m]->name;
      $html .= "<option value=" . $mod_id . ">" . $mod_name . "</option>";
    }
    $html .= "</select>";
    print $html;
  }
}

/**
 * Add Page.
 */
function drupal_sugar_portal_bcp_add_module() {

  global $_drupal_sugar_portal_objSCP, $_drupal_sugar_portal_template_path;
  if (is_object($_drupal_sugar_portal_objSCP)) {
    $html = "";
    include $_drupal_sugar_portal_template_path . 'bcp_add_page_v7.php';
    return $html;
  }
  else {
    $_drupal_sugar_portal_objSCP = 0;
    $conn_err = "Connection with CRM not successful. Please check CRM Version, URL, Username and Password.";
    setcookie('bcp_connection_error', $conn_err, time() + 60, '/');
  }
}

/**
 * Add form data.
 */
function drupal_sugar_portal_bcp_add_moduledata_call() {
  global $_drupal_sugar_portal_objSCP;
  if (is_object($_drupal_sugar_portal_objSCP)) {
    $pass_name_arry = array(
      'name' => $_REQUEST['add-name'],
      'description' => $_REQUEST['add-description'],
      'status' => 'New',
      'priority' => $_REQUEST['add-priority'],
      'type' => $_REQUEST['add-type'],
    );

    $new_id = $_drupal_sugar_portal_objSCP->setEntry('Cases', $pass_name_arry);
    // Set relationship with contact.
    $_drupal_sugar_portal_objSCP->set_relationship('Contacts', $_SESSION['scp_user_id'], 'cases', $new_id);
    $redirect_url = 'list-Cases';
    $conn_err = "Case added successfully.";
    setcookie('bcp_add_record', $conn_err, time() + 30, '/');
    return $redirect_url;
  }
  else {
    $_drupal_sugar_portal_objSCP = 0;
    $conn_err = "Connection with CRM not successful. Please check CRM Version, URL, Username and Password.";
    setcookie('bcp_connection_error', $conn_err, time() + 60, '/');
  }
}
